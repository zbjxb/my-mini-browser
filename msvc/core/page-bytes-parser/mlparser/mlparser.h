#ifndef MLPARSER_H__
#define MLPARSER_H__

#include "../markuplang_def.h"

struct mlparser {
	virtual bool Parse(const char* file);					// 解析整棵文档树

	mlparser() { File = NULL; }

	virtual void Init(const char* file);					// 解析前初始化工作
	virtual void GetChar();									// 从锅里捞一碗
	virtual void SkipWhite();								// 过滤老鼠屎
	virtual void Abort(const char*);						// 出错停止
	virtual void Expected(const char*);						// 提示信息
	virtual void Match(char);								// 期待某个字符，不符合期待就失败
	virtual void MatchString(const char*);					// 期待指定字符串
	virtual std::string GetElemName();						// 获取元素名
	virtual std::string GetElemValue();						// 获取元素值
	virtual std::string GetAttribName();					// 获取属性名
	virtual std::string GetAttribValue();					// 获取属性值

	virtual void GetElement(mlnode *parent);				// 获取元素
	virtual void GetAttrib(mlnode *node);					// 获取属性
	virtual bool Ignored();									// 是否有忽略不常用的信息，比如声明、文档验证信息、注释
	virtual void IgnoreDecl();				                // 忽略声明
	virtual void IgnoreValid();				                // 忽略文档验证信息
	virtual void IgnoreComment();			                // 忽略注释

	char Look;						// 碗
	FILE *File;						// 锅
	mlnode	Tree;					// 树
};

#endif // MLPARSER_H__
