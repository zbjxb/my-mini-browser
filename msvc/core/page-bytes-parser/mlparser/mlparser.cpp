#include "stdafx.h"
#include "mlparser.h"

void mlparser::Init( const char* file ) {
	// 开锅
	fopen_s(&File, file, "rb");
	if (File == NULL)
		Expected("File open normally");
	// 先捞一碗
	GetChar();
	// 过滤老鼠屎
	SkipWhite();
}

void mlparser::Expected( const char* s ) {
	std::string info(s);
	info.append(" Expected.");
	Abort(info.c_str());
}

void mlparser::Abort( const char* s ) {
	printf("\nError: %s.\n", s);

	if (File != NULL)
		printf("Crash location: %d\n", (int)ftell(File));

	printf("Press ENTER to exit...");
	system("pause");
	exit(-1);
}

void mlparser::GetChar() {
	Look = char(fgetc(File));
	//printf("%c",Look);
}

void mlparser::SkipWhite() {
	while (isspace(Look))
		GetChar();
}

void mlparser::Match( char ch ) {
	if (Look == ch) {
		GetChar();
		SkipWhite();
	}
	else {
		char info[] = {"'''''''"};
		info[3] = ch;
		Expected(info);
	}
}

void mlparser::MatchString( const char* str ) {
	for (const char *p = str; *p != 0; p++) {
		if (*p == Look)
			GetChar();
		else
			Expected(p);
	}
	SkipWhite();
}

std::string mlparser::GetElemName() {
	if (!isalpha(Look)) {
		Expected("Name");
	}
	char name[30] = {0};
	char *p = name;
	while (isalnum(Look) || (!isspace(Look) && (Look != '>'))) {
		*p = Look;
		p++;
		GetChar();
	}
	SkipWhite();
	return name;
}

std::string mlparser::GetElemValue() {
	std::string elem_value;
	while (Look != '<') {
		elem_value.append(1, Look);
		GetChar();
	}
	return elem_value;
}

std::string mlparser::GetAttribName() {
	if (!isalpha(Look)) {
		Expected("Name");
	}
	char name[30] = {0};
	char *p = name;
	while (isalnum(Look) || (!isspace(Look) && (Look != '='))) {
		*p = Look;
		p++;
		GetChar();
	}
	SkipWhite();
	return name;
}

std::string mlparser::GetAttribValue() {
	std::string attrib_value;
	if (Look == '\'' || Look == '"') {
		char mode = Look;
		Match(mode);
		while (Look != mode) {
			attrib_value.append(1, Look);
			GetChar();
		}
		Match(mode);
	}
	else {
		Expected("Attribute Value");
	}
	return attrib_value;
}

void mlparser::GetElement( mlnode *parent ) {
	do {
		mlnode node;
		Match('<');
		while (Ignored()) {
			Match('<');
		}
		node.elem_name = GetElemName();
		while (Look != '/' && Look != '>') {
			// 解析属性
			GetAttrib(&node);
		}
		if (Look == '/') {
			Match('/');
			Match('>');
		}
		else if (Look == '>') {
			Match('>');
			if (Look != '<') {
				node.elem_value.append(GetElemValue());
			}
			fpos_t pos;
			fgetpos(File, &pos);
			Match('<');
			while (Ignored()) {
				if (Look != '<' && Look != EOF) {
					parent->elem_value.append(GetElemValue());
				}
				fgetpos(File, &pos);
				Match('<');
			}
			if (Look != '/') {
				// 子节点
				fsetpos(File, &pos);
				Look = '<';
				GetElement(&node);
				if (Look != '<') {
					parent->elem_value.append(GetElemValue());
				}
				Match('<');
				while (Ignored()) {
					if (Look != '<' && Look != EOF) {
						parent->elem_value.append(GetElemValue());
					}
					fgetpos(File, &pos);
					Match('<');
				}
			}

			Match('/');
			MatchString(node.elem_name.c_str());
			Match('>');
		}
		else {
			Expected("Closed Brackets needed");
		}
		parent->childs.push_back(node);

		if (Look != '<' && Look != EOF) {
			parent->elem_value.append(GetElemValue());
		}
		if (Look != EOF) {
			fpos_t pos;
			fgetpos(File, &pos);
			Match('<');
			while (Ignored()) {
				if (Look != '<' && Look != EOF) {
					parent->elem_value.append(GetElemValue());
				}
				if (Look != EOF) {
					fgetpos(File, &pos);
					Match('<');
				}
			}
			if (Look == EOF) {
				return;
			}
			if (Look != '/') {
				fsetpos(File, &pos);
				Look = '<';
			}
			else {
				fsetpos(File, &pos);
				Look = '<';
				break;
			}
		}

	} while (Look != EOF);
}

void mlparser::GetAttrib( mlnode *node ) {
	Attrib_st attrib;
	attrib.attrib_name = GetAttribName(); 
	Match('=');
	attrib.attrib_value = GetAttribValue();
	node->attribs.push_back(attrib);
}

bool mlparser::Ignored() {
	if (Look == '?') {
		IgnoreDecl();
	}
	else if (Look == '!') {
		Match('!');
		if (Look == '-') {
			IgnoreComment();
		}
		else {
			IgnoreValid();
		}
	}
	else {
		return false;
	}
	return true;
}

void mlparser::IgnoreDecl() {
	IgnoreComment();
}

void mlparser::IgnoreValid() {
	while (Look != '>') {
		GetChar();
		if (Look == '[') {
			while (Look != ']') {
				GetChar();
			}
		}
	}
	Match('>');
}

void mlparser::IgnoreComment() {
	while (Look != '>') {
		GetChar();
	}
	Match('>');
}

bool mlparser::Parse( const char* file ) {
	Init(file);
	GetElement(&Tree);
	fclose(File);
	return (!Tree.childs.empty());
}

