#ifndef MARKUPLANG_DEF_H__
#define MARKUPLANG_DEF_H__

#include <string>
#include <vector>

struct mlnode;
struct Attrib_st {
	std::string attrib_name;
	std::string attrib_value;
};

typedef std::vector<Attrib_st>  Attribs;
typedef std::vector<mlnode>    Nodes;

struct mlnode {
    //std::string doctype;
	std::string elem_name;
	std::string elem_value;
	Attribs attribs;
	Nodes childs;
};

#endif // MARKUPLANG_DEF_H__
