#ifndef PARSE_ARGUMENTS_H__
#define PARSE_ARGUMENTS_H__

#include <ostream>
#include <string>
#include <map>
#include <vector>

struct parse_arguments {

    void registerHelpDoc( const tchar* doc );
    bool parse(int argc, tchar** argv);
    bool contains( tchar argname ) const;
    bool contains( const tchar* argname ) const;
    const tchar* argval( tchar argname ) const;
    const tchar* argval( const tchar* argname ) const;
    tstring error_msg() const;

    void echoargs( tostream& out ) const;

    typedef std::map<tstring, tstring> StrNVMap;
    StrNVMap nv;

    tstring helpdoc;
    std::vector<tstring> errmsg;
};

#endif // PARSE_ARGUMENTS_H__
