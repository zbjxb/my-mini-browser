#ifndef MYTYPES_H__
#define MYTYPES_H__

#include <tchar.h>
#include <string>
#include <iostream>

typedef TCHAR tchar;

#ifdef UNICODE
typedef std::wstring tstring;
typedef std::wostream tostream;
#define tcout std::wcout
#else // ANSI
typedef std::string tstring;
typedef std::ostream tostream;
#define tcout std::cout
#endif // UNICODE

#endif // MYTYPES_H__
