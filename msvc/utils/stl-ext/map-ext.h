#ifndef STL_EXT_H__
#define STL_EXT_H__

#include <map>

template <typename kT, typename vT>
bool map_contains( const std::map<kT, vT>& mapref, const kT& key ) {
    return mapref.find( key ) != mapref.end();
}

#endif // STL_EXT_H__
