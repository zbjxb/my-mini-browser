objs = main.o parse-arguments.o map-ext.o

mi.exe: $(objs)
	g++ -Wall -g $^ -o $@

main.o: main.cpp
vpath %.cpp utils/parse-arguments
vpath %.cpp utils/stl-ext

%.o: %.cpp
	g++ -Wall -g -c $< -o $@

.PHONY: clean cleanall
cleanall: clean
	-rm mi.exe
clean:
	-rm $(objs)
